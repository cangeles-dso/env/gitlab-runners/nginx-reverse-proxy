#!/bin/bash
set -e -x

SCRIPT_DIR="$( cd "$(dirname "${BASH_SOURCE[0]}")" >/dev/null 2>&1 ; pwd -P )"
DOCKER_GROUP=$(getent group docker | cut -d: -f1-3)
NGINX_GROUP=$(docker run --rm nginx cat /etc/group)

## Create directories
if [ ! -d "$SCRIPT_DIR/nginx-conf.d" ] && [ ! -d "$SCRIPT_DIR/nginx-etc" ];
then
    mkdir $SCRIPT_DIR/nginx-conf.d;
    mkdir $SCRIPT_DIR/nginx-etc;
fi

## Create nginx default.conf
cat << EOF > $SCRIPT_DIR/nginx-conf.d/default.conf
server {
    listen       2375;
    set \$resp '403 Forbidden\n';

    location ~ (/.*|)/(stop|network|volume|config) {
        return 403 \$resp;
    }

    location ~ (/.*|)/containers/(.*/attach|.*/restart|prune) {
        return 403 \$resp;
    }

    location / {
        proxy_pass http://unix:/tmp/docker.sock:;
        proxy_set_header    X-Real-IP           \$remote_addr;
        proxy_set_header    X-Forwarded-For     \$proxy_add_x_forwarded_for;
        proxy_set_header    X-Forwarded-Proto   \$scheme;
        proxy_set_header    Host                \$host;
        proxy_set_header    X-Forwarded-Host    \$host;
        proxy_set_header    X-Forwarded-Port    \$server_port;
        proxy_set_header    Upgrade             \$http_upgrade;
        proxy_redirect                          off;
    }
}
EOF

## Create docker group for nginx
cat << EOF > $SCRIPT_DIR/nginx-etc/group
$NGINX_GROUP
$DOCKER_GROUP:nginx,www-data
EOF

## Start nginx container
if [ -d "$SCRIPT_DIR/nginx-conf.d" ] && [ -d "$SCRIPT_DIR/nginx-etc" ];
then
    docker run --rm --name nginx-proxy -d -p 2375:2375 \
        -v $SCRIPT_DIR/nginx-conf.d:/etc/nginx/conf.d \
        -v $SCRIPT_DIR/nginx-etc/group:/etc/group:ro \
        -v /var/run/docker.sock:/tmp/docker.sock:ro \
        nginx;
fi